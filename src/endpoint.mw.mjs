import logger from './logger.mjs';

/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    const endpoint = `[ENDPOINT] ${req.method} '${req.originalUrl}'`;
    logger.info(endpoint);
    
    next();
};

export default endpoint_mw;