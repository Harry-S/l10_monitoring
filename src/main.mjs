import logger from "./logger.mjs";
import app from "./router.mjs";

const PORT = 3000;
const HOSTNAME = "127.0.0.1";

// Log
// Router

const main = () => {
    logger.log("info", "Service starting.")
    // temp error
    // logger.log("error", "This is an error.")
    app.listen(PORT, HOSTNAME, () => {
        logger.log("info", `Listening at http://${HOSTNAME}:${PORT}`);
    })
}

main();